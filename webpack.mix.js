const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
/*.js('node_modules/paper-dashboard-2/js/jquery.min.js', 'public/js')
.js('node_modules/paper-dashboard-2/js/bootstrap-notifly.js', 'public/js')
.js('node_modules/paper-dashboard-2/js/chartjs.min.js', 'public/js')
.js('node_modules/paper-dashboard-2/js/perfect-scrollbar.jquery.js', 'public/js')
.css('node_modules/paper-dashboard-2/css/paper-dashboard.css', 'public/css');*/