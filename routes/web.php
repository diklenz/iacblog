<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/blog/category/{slug?}', 'BlogController@category')->name('category');
Route::get('/blog/page/{slug?}', 'BlogController@page')->name('page');
Route::get('/blog/article/{slug?}', 'BlogController@article')->name('article');
Route::get('/blog/search', 'BlogController@search')->name('search');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    Route::resource('/category', 'CategoryController', ['as' => 'admin']);
    Route::resource('/page', 'PageController', ['as' => 'admin']);
    Route::resource('/article', 'ArticleController', ['as' => 'admin']);
    Route::get('/options', 'OptionsController@index')->name('admin.option.index');
    Route::get('/options/edit', 'OptionsController@edit')->name('admin.option.edit');

    Route::group(['prefix' => 'user_managment', 'namespace' => 'UserManagment'], function () {
        Route::resource('/user', 'UserController', ['as' => 'admin.user_managment']);
        Route::resource('/role', 'RoleController', ['as' => 'admin.user_managment']);
    });


});

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();
