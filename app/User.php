<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role');
    }

    public function isAdministrator()
    {
        return $this->roles()->where('name', 'Administrator')->exists();
    }
    /*   public function isEmployee()
       {
           $roles = $this->roles->toArray();
           return !emply($roles);
       }

       public function hasRole($check)
       {
           return in_array($check, array_pluck($this->roles->toArray(), 'name'));

       }

       private function getIdInArray($array, $term)
       {
           foreach ($array as $key => $value) {
               if ($value == $term) {
                   return $key + 1;
               }
           }
           return false;
       }

       public function makeEmployee($title)
       {
           $assigned_roles = array();
           $roles = array_fetch(Role::all()->toArray(), 'name');
           switch ($title) {
               case 'admin':
                   $assigned_roles[] = $this->getIdInArray($roles, 'admin');
               case 'client':
                   $assigned_roles[] = $this->getIdInArray($roles, 'client');
                   break;
               default:
                   $assigned_roles[] = false;
           }
           $this->roles()->attach($assigned_roles);
       }*/
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
