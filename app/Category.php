<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    // Mass assigned
    protected $fillable = ['title', 'slug', 'parent_id', 'published', 'created_by', 'modified_by', 'description', 'image', 'image_show'];
    // Mutators
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40) . "-" . Carbon::now()->format('dmyHi'), '-');
    }

    // Get children category
    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    // Polymorphic relation with articles
    public function articles()
    {
        return $this->morphedByMany('App\Article', 'categoryable');
    }

    public function pages()
    {
        return $this->morphedByMany('App\Page', 'categoryable');
    }


    public function scopeLastCategories($query, $count)
    {
        return $query->orderBy('created_at', 'desc')->take($count)->get();
    }
}
