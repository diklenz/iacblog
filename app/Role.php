<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $fillable = [
        'id', 'name'
    ];
    public function users()
    {
        //return $this->morphedByMany(User::class, 'user_role');
        return $this->belongsToMany(User::class, 'user_role');
    }
}
