<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use App\Page;
use App\User;

class DashboardController extends Controller
{
    // Dashboard
    public function dashboard()
    {
        return view('admin.dashboard', [
            'categories' => Category::LastCategories(5),
            'articles' => Article::LastArticles(5),
            'pages' => Page::LastPages(5),
            'count_categories' => Category::count(),
            'count_articles' => Article::count(),
            'count_pages' => Page::count(),
            'count_users' => User::count()
        ]);
    }
}
