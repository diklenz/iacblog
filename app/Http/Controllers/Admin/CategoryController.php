<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.categories.index', [
            'categories' => Category::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.categories.create', [
            'category' => [],
            'categories' => Category::with('children')->where('parent_id', '0')->get(),
            'delimiter' => ''
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate(
            ['title' => ['required', 'string', 'max:255'],


            ]
        );
        $category = Category::create($request->all());
        //img

        if ($request->hasFile('image')):
            $path = $request->image->store('uploads', 'public');
            $category->image = $path;
            $category->save();
        endif;
        return redirect()->route('admin.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return Response
     */
    public function edit(Category $category)
    {
        //

        return view('admin.categories.edit', [
            'category' => $category,
            'categories' => Category::with('children')->where('parent_id', '0')->get(),
            'delimiter' => ''
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $validator = $request->validate(
            ['title' => ['required', 'string', 'max:255'],
                'slug' => ['required', 'string', 'max:255'],


            ]
        );
        if (($request->hasFile('image')) && ($category->image != null) && (file_exists(public_path() . '/storage/' . $category->image))):
            unlink(public_path('/storage/' . $category->image));
            $category->image = null;
        endif;
        $category->update($request->except('slug'));
        if ($request->hasFile('image')):

            $path = $request->image->store('uploads', 'public');
            $category->image = $path;
            $category->save();
        endif;
        return redirect()->route('admin.category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return Response
     */
    public function destroy(Category $category)
    {
        //
        if (($category->image != null) && (file_exists(public_path() . '/storage/' . $category->image))):
            unlink(public_path('/storage/' . $category->image));
        endif;
        $category->delete();
        return redirect()->route('admin.category.index');
    }
}
