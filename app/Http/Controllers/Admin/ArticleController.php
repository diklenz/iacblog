<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        if ($request->input('searching')):
            $article = Article::where('title', 'LIKE', '%' . $request->input('searching') . '%')->paginate(10);
        elseif ($request->input('category_search')):
            if (($request->input('category_search')) == 0):
                $article = Article::orderBy('created_at', 'desc')->paginate(10);
            else:
                $category = Category::where('id', $request->input('category_search'))->first();
                $article = $category->articles()->paginate(20);
            endif;
        elseif ($request->input('category_published')):
            if (($request->input('category_published')) == 1):
                $article = Article::orderBy('created_at', 'desc')->where('published', 1)->paginate(10);
            elseif (($request->input('category_published')) == 0):
                $article = Article::where('published', 0)->paginate(10);
            endif;
        else:
            $article = Article::orderBy('created_at', 'desc')->paginate(10);
        endif;

        return view('admin.articles.index', [
            'articles' => $article,
            'categories' => Category::orderBy('created_at', 'desc')->paginate(10)

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('admin.articles.create', [
            'article' => [],
            'categories' => Category::with('children')->where('parent_id', 0)->get(),
            'delimiter' => ''


        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $validator = $request->validate(
            ['title' => ['required', 'string', 'max:255'],
                'description_short' => ['required'],
                'description' => ['required'],

            ]
        );
        $article = Article::create($request->all());

        //Cat
        if ($request->input('categories')):
            $article->categories()->attach($request->input('categories'));
        endif;
        //img
        if ($request->hasFile('image')):
            $path = $request->image->store('uploads', 'public');
            $article->image = $path;
            $article->save();
        endif;
        return redirect()->route('admin.article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Article $article
     * @return Response
     */
    public function edit(Article $article)
    {
        //
        return view('admin.articles.edit', [
            'article' => $article,
            'categories' => Category::with('children')->where('parent_id', 0)->get(),
            'delimiter' => ''
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Article $article
     * @return Response
     */
    public function update(Request $request, Article $article)
    {
        $validator = $request->validate(
            ['title' => ['required', 'string', 'max:255'],
                'slug' => ['required', 'string', 'max:255'],
                'description_short' => ['required'],
                'description' => ['required'],

            ]
        );
        if (($request->hasFile('image')) && ($article->image != null) && (file_exists(public_path() . '/storage/' . $article->image))):
            unlink(public_path('/storage/' . $article->image));
            $article->image = null;
        endif;
        $article->update($request->except('slug'));


        $article->categories()->detach();
        if ($request->input('categories')):
            $article->categories()->attach($request->input('categories'));
        endif;

        //Img
        if ($request->hasFile('image')):

            $path = $request->image->store('uploads', 'public');
            $article->image = $path;
            $article->save();
        endif;
        return redirect()->route('admin.article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Article $article
     * @return Response
     */
    public function destroy(Article $article)
    {
        //
        $article->categories()->detach();
        if (($article->image != null) && (file_exists(public_path() . '/storage/' . $article->image))):
            unlink(public_path('/storage/' . $article->image));
        endif;
        $article->delete();
        return redirect()->route('admin.article.index');
    }


}
