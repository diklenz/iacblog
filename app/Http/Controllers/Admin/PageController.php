<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //

        if ($request->input('searching')):
            $page = page::where('title', 'LIKE', '%' . $request->input('searching') . '%')->paginate(10);
        elseif ($request->input('category_search')):
            if (($request->input('category_search')) == 0):
                $page = Page::orderBy('created_at', 'desc')->paginate(10);
            else:
                $category = Category::where('id', $request->input('category_search'))->first();
                $page = $category->articles()->paginate(20);
            endif;
        elseif ($request->input('category_published')):
            if (($request->input('category_published')) == 1):
                $page = Page::orderBy('created_at', 'desc')->where('published', 1)->paginate(10);
            else:
                $page = Page::orderBy('created_at', 'desc')->where('published', null)->paginate(10);
            endif;
        else:
            $page = page::orderBy('created_at', 'desc')->paginate(10);
        endif;

        return view('admin.pages.index', [
            'pages' => $page,
            'categories' => Category::orderBy('created_at', 'desc')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('admin.pages.create', [
            'page' => [],
            'categories' => Category::with('children')->where('parent_id', 0)->get(),
            'delimiter' => ''


        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $validator = $request->validate(
            ['title' => ['required', 'string', 'max:255'],
                'description_short' => ['required'],
                'description' => ['required'],

            ]
        );
        $page = page::create($request->all());

        //Cat
        if ($request->input('categories')):
            $page->categories()->attach($request->input('categories'));
        endif;
        //img
        if ($request->hasFile('image')):
            $path = $request->image->store('uploads', 'public');
            $page->image = $path;
            $page->save();
        endif;
        return redirect()->route('admin.page.index');
    }

    /**
     * Display the specified resource.
     *
     * @param page $page
     * @return Response
     */
    public function show(page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param page $page
     * @return Response
     */
    public function edit(page $page)
    {
        //
        return view('admin.pages.edit', [
            'page' => $page,
            'categories' => Category::with('children')->where('parent_id', 0)->get(),
            'delimiter' => ''
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param page $page
     * @return Response
     */
    public function update(Request $request, page $page)
    {
        $validator = $request->validate(
            ['title' => ['required', 'string', 'max:255'],
                'slug' => ['required', 'string', 'max:255'],
                'description_short' => ['required'],
                'description' => ['required'],

            ]
        );
        if (($request->hasFile('image')) && ($page->image != null) && (file_exists(public_path() . '/storage/' . $page->image))):
            unlink(public_path('/storage/' . $page->image));
            $page->image = null;
        endif;

        $page->update($request->except('slug'));


        $page->categories()->detach();
        if ($request->input('categories')):
            $page->categories()->attach($request->input('categories'));
        endif;

        //Img
        if ($request->hasFile('image')):

            $path = $request->image->store('uploads', 'public');
            $page->image = $path;
            $page->save();
        endif;
        return redirect()->route('admin.page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param page $page
     * @return Response
     */
    public function destroy(page $page)
    {
        //
        $page->categories()->detach();
        if (($page->image != null) && (file_exists(public_path() . '/storage/' . $page->image))):
            unlink(public_path('/storage/' . $page->image));
        endif;
        $page->delete();
        return redirect()->route('admin.page.index');
    }


}
