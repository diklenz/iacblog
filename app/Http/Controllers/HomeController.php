<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*  public function __construct()
      {
          $this->middleware('auth');
      }*/

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $main_category = Category::where('title', 'Главная')->first();
        $category = Category::where('title', 'Новости')->first();
        return view('blog.home', [
            'project_articles' => $main_category->articles()->where('published', 1)->paginate(5),
            'articles' => $category->articles()->where('published', 1)->paginate(20),
        ]);
    }
}
