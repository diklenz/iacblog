<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Page;
use App\Providers\Event;
use App\User;
use Illuminate\Support\Facades\Input;

class BlogController extends Controller
{
    //
    public function category($slug)
    {
        $category = Category::where('slug', $slug)->first();
        return view('blog.category', [
            'category' => $category,
            'articles' => $category->articles()->where('published', 1)->paginate(10)
        ]);

    }

    public function article($slug)
    {
        $article = Article::where('slug', $slug)->first();
        $article->increment('viewed', 1);
        return view('blog.article', [
            'article' => $article,
            'user' => User::where('id', $article->created_by)->first()
        ]);
    }

    public function page($slug)
    {
        $page = Page::where('slug', $slug)->first();
        $page->increment('viewed', 1);
        return view('blog.page', [
            'page' => $page,
            'user' => User::where('id', $page->created_by)->first()
        ]);
    }

    public function search()
    {
        $title = Input::get('searching', ' ');
        //$term = Input::get('term', false);
        return view('blog.search', [
            'search' => $title,
            'articles' => Article::where('title', 'LIKE', '%' . $title . '%')->where('published', 1)->paginate(10)
        ]);
    }
}
