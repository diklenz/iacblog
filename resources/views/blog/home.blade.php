@extends('layouts.app')

@section('content')
    <div class="position-fixed pos-sm-relative w-100 h-100 vh-100">
        <div class="abs-blr pos-sm-relative h-100 vh-100">
            <div class="swiper-container h-100 vh-100 overflow-hidden" data-slide-effect="slide" data-autoheight="true"
                 data-swiper-speed="500" data-swiper-margin="0" data-swiper-slides-per-view="1"
                 data-swiper-breakpoints="true" data-scrollbar="true" data-swiper-loop="true"
                 data-swpr-responsive="[1, 1, 1, 1]" data-swiper-direction="vertical">
                <div class="swiper-pagination"></div>
                <div class="swiper-wrapper">
                    @forelse($project_articles as $article)

                        <div class=" swiper-slide slider-main  pos-relative pt-95 pb-25">
                            <div class="img-bg bg bg-layer-4"
                                 style="background-image: url('/storage/{{$article->image}}')"></div>
                            <div class="container-fluid h-100 mt-xs-50">

                                <div class="row h-100">
                                    <div class="col-md-1 d-md-block d-xs-none"></div>
                                    <div class="col-sm-12 col-md-5 h-100 h-sm-50">
                                        <div class="dplay-tbl">
                                            <div class="dplay-tbl-cell color-white mtb-30">
                                                <div class="mx-w-400x">
                                                    <h5><b>{{$article->meta_keyword}}</b></h5>
                                                    <h1 class="mt-20 mb-30"><b>{{$article->title}}</b></h1>
                                                    <div class="mt-20 mb-30 color-white text-desc-home"
                                                    >
                                                        <style>
                                                            .text-desc-home *, .text-desc-home > *, .text-desc-home p {
                                                                color: #fff !important;
                                                            }
                                                        </style>{!! $article->description_short !!}</div>
                                                    <h6><a class="plr-20 btn-brdr-grey color-white"
                                                           href="{{route('article', $article->slug)}}"><b>Продолжить
                                                                чтение</b></a></h6>
                                                </div><!-- mx-w-200x -->
                                            </div><!-- dplay-tbl-cell -->
                                        </div><!-- dplay-tbl -->
                                    </div><!-- col-sm-6 -->


                                </div><!-- row -->
                            </div><!-- container -->
                        </div><!-- slider-main -->

                    @empty
                        <h2>Пусто</h2>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid pos-sm-relative  h-100 mt-xs-50">

        <div class="row h-100">
            <div class="col-md-1"></div>
            <div class="col-sm-12 col-md-4 h-100 h-sm-50"></div>
            <div class="col-sm-12 col-md-6 h-sm-50 overflow-hidden pos-relative mt-100 ">
                <div class="row pos-relative all-scroll h-100">
                    @forelse($articles as $article)
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6  mb-30">
                                <div class="bg-white">
                                    <img src="/storage/{{$article->image}}" alt="">

                                    <div class="plr-25 ptb-15">
                                        <h5 class="color-ash"><b>{{$article->meta_keyword}}</b></h5>
                                        <h4 class="mtb-10">
                                            <a href="{{route('article', $article->slug)}}"><b>{{$article->title}}</b></a>
                                        </h4>

                                        {!! $article->description_short !!}

                                    </div><!-- hot-news -->
                                </div><!-- hot-news -->
                            </div><!-- swiper-slide -->
                    @empty
                        <h2>Пусто</h2>
                    @endforelse
                </div>
            </div><!-- col-sm-6 -->
            <div class="col-md-1"></div>
        </div>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
@endsection
