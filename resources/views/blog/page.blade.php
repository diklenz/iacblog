@extends('layouts.app')

@section('title', $page->title)
@section('meta-keyword', $page->meta_keyword)
@section('meta_description', $page->meta_description)
@section('content')
    <div class="slider-main h-500x h-sm-auto pos-relative pt-95 pb-25">
        <div class="img-bg bg bg-layer-4" style="background-image: url('/storage/{{ $page->image ?? ''}}')"></div>
        <div class="container-fluid h-100 mt-xs-50">
            <div class="dplay-tbl">
                <div class="dplay-tbl-cell color-white text-center">

                    <h1 class="ptb-50"><b>{{$page->title}}</b></h1>

                </div><!-- dplay-tbl-cell -->
            </div><!-- dplay-tbl -->
        </div><!-- container -->
    </div>

    <section class="bg-1-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">

                    <div class="">

                        <div class="s-right">
                            <p class="ptb-20 color-ash"><b> Опубликовал {{$user->name}}.<BR> Дата: {{$page->created_at}}
                                </b></p>
                        </div>
                    </div><!-- sided-80x-->
                </div><!-- col-md-6-->

                <div class="col-sm-12 col-md-6">
                    <ul class="color-ash lh-70 text-right text-sm-left list-a-plr-10 font-13">
                        <li><b>Количество просмотров</b></li>
                        <li> {{$page->viewed}}</li>

                    </ul>
                </div><!-- col-md-6-->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="bg-white p-50">
                        {!! $page->description !!}
                    </div><!-- col-sm-3 -->
                </div><!-- col-sm-3 -->
            </div><!-- row -->
        </div><!-- container -->
    </section>


@endsection