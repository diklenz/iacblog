@extends('layouts.app')

@section('title', $category->title)
@section('content')
    <div class="slider-main h-500x h-sm-auto pos-relative pt-95 pb-25">
        <div class="img-bg bg bg-layer-4" style="background-image: url('/storage/{{ $category->image ?? ''}}')"></div>
        <div class="container-fluid h-100 mt-xs-50">
            <div class="dplay-tbl">
                <div class="dplay-tbl-cell color-white text-center">

                    <h1 class="ptb-50"><b>{{$category->title}}</b></h1>

                </div><!-- dplay-tbl-cell -->
            </div><!-- dplay-tbl -->
        </div><!-- container -->
    </div>
    <section class="bg-1-white ptb-0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-md-12 col-lg-10 ptb-50 pr-30 pr-md-15">


                    <div class="row">
                        @forelse($articles as $article)
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6  mb-30">

                                <div class="card h-100 h-xs-500x">
                                    <div class="sided-half sided-xs-half h-100 bg-white">

                                        <!-- SETTING IMAGE WITH bg-15 -->
                                        <div class="s-left w-50 w-xs-100 h-100 h-xs-50 pos-relative">
                                            <div class="bg abs-tblr"
                                                 style="background-image: url('/storage/{{$article->image}}')"></div>
                                        </div>

                                        <div class="s-right w-50 w-xs-100 h-xs-50">
                                            <div class="plr-25 ptb-25 h-100">
                                                <div class="dplay-tbl">
                                                    <div class="dplay-tbl-cell">

                                                        <h5 class="color-ash"><b>НОВОСТЬ</b></h5>
                                                        <h3 class="mtb-10"><a
                                                                    href="{{route('article', $article->slug)}}">
                                                                <b>{{$article->title}}</b></a></h3>
                                                        <p>
                                                        <p>{!! $article->description_short !!}</p></p>
                                                        <ul class="list-li-mr-10 color-lt-black">
                                                            <li>
                                                                <i class="mr-5 font-12 icon ion-ios-eye ion-android-eye"></i>{{$article->viewed}}
                                                            </li>
                                                            <!--li><i class="mr-5 font-12 ion-ios-chatbubble-outline"></i>105
                                                            </li-->
                                                        </ul>

                                                    </div><!-- dplay-tbl-cell -->
                                                </div><!-- dplay-tbl -->
                                            </div><!-- plr-25 ptb-25 -->
                                        </div><!-- s-right -->
                                    </div><!-- sided-half -->
                                </div><!-- card -->
                            </div>
                        @empty
                            <h2>Пусто</h2>
                        @endforelse

                        <div class="links pull-right">{{$articles->links()}}
                        </div>


                    </div><!-- col-sm-9 -->


                </div><!-- row -->
            </div><!-- container -->
    </section>

@endsection