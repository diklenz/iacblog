@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
<label for="">Статус</label>
<select class="form-control" name="published">
    @if (isset($article->id))
        <option value="0" @if ($article->published == 0) selected="" @endif>Не опубликовано</option>
        <option value="1" @if ($article->published == 1) selected="" @endif>Опубликовано</option>
    @else
        <option value="0">Не опубликовано</option>
        <option value="1">Опубликовано</option>
    @endif
</select>

<label for="">Заголовок</label>
<input type="text" class="form-control" name="title" placeholder="Заголовок категории"
       value="{{$article->title ?? ""}}" required>

<label for="">Slug</label>
<input class="form-control" type="text" name="slug" placeholder="Автоматическая генерация"
       value="{{$article->slug ?? ""}}" readonly="">

<label for="">Родительская категория</label>
<select class="form-control" name="categories[]" multiple="">
    <option value="0">-- без родительской категории --</option>
    @include('admin.articles.partials.categories', ['categories' => $categories])
</select>
<label for="description_short">Краткое описание</label>
<textarea name="description_short" class="form-control" id="description_short" cols="30"
          rows="10">{{$article->description_short ??""}}</textarea>

<label for="description">Текст новости</label>
<textarea name="description" class="form-control" id="description" cols="30"
          rows="10">{{$article->description ??""}}</textarea>
<hr/>
<label for="">Изображение</label>
@if (isset($article->image))
    <img class="img-fluid" name="image_img" src="{{asset('/storage/'. $article->image ?? "")}}">
@endif
<input type="file" class="form-control" name="image">
<label for="">Показ изображения:</label>
<select class="form-control" name="image_show">
    @if (isset($article->id))
        <option value="0" @if ($article->image_show == 0) selected="" @endif>Не опубликовано</option>
        <option value="1" @if ($article->image_show == 1) selected="" @endif>Опубликовано</option>
    @else
        <option value="0">Не опубликовано</option>
        <option value="1">Опубликовано</option>
    @endif
</select>
<hr/>
<label for="">Мета Заголовок</label>
<input type="text" class="form-control" name="meta_title" placeholder="Мета заголовок"
       value="{{$article->meta_title ?? ""}}">

<label for="">Мета описание</label>
<input type="text" class="form-control" name="meta_description" placeholder="Мета описание"
       value="{{$article->meta_description ?? ""}}">

<label for="">Ключевые слова</label>
<input type="text" class="form-control" name="meta_keyword" placeholder="Ключевые слова"
       value="{{$article->meta_keyword ?? ""}}">

<hr/>
<input class="btn btn-primary" type="submit" value="Сохранить">
