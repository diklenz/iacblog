@foreach ($categories as $category)

    <option value="{{$category->id ?? ""}}"

            @isset($page->id)
            @foreach ($page->categories as $category_page)
            @if ($category->id == $category_page->id)

            selected="selected"
            @endif
            @endforeach
            @endisset

    >
        {!! $delimiter ?? "" !!}{{$category->title ?? ""}}
    </option>

    @if (count($category->children) > 0)

        @include('admin.pages.partials.categories', [
          'categories' => $category->children,
          'delimiter'  => ' - ' . $delimiter
        ])

    @endif
@endforeach
