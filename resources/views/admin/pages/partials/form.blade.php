@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

<label for="">Статус</label>
<select class="form-control" name="published">
    @if (isset($page->id))
        <option value="0" @if ($page->published == 0) selected="" @endif>Не опубликовано</option>
        <option value="1" @if ($page->published == 1) selected="" @endif>Опубликовано</option>
    @else
        <option value="0">Не опубликовано</option>
        <option value="1">Опубликовано</option>
    @endif
</select>

<label for="">Заголовок</label>
<input type="text" class="form-control" name="title" placeholder="Заголовок категории"
       value="{{$page->title ?? ""}}" required>

<label for="">Slug</label>
<input class="form-control" type="text" name="slug" placeholder="Автоматическая генерация"
       value="{{$page->slug ?? ""}}" readonly="">
<label for="">Вложение</label>
<select class="form-control" name="categories[]" multiple="">
    <option value="0">-- без вложения --</option>
    @include('admin.pages.partials.categories', ['categories' => $categories])
</select>
<label for="description_short">Краткое описание</label>
<textarea name="description_short" class="form-control" id="description_short" cols="30"
          rows="10">{{$page->description_short ??""}}</textarea>

<label for="description">Текст статьи</label>
<textarea name="description" class="form-control" id="description" cols="30"
          rows="10">{{$page->description ??""}}</textarea>
<hr/>
<label for="">Изображение</label>
@if (isset($page->image))
    <img class="img-fluid" name="image_img" src="{{asset('/storage/'. $page->image ?? "")}}">
@endif
<input type="file" class="form-control" name="image">
<label for="">Показ изображения:</label>
<select class="form-control" name="image_show">
    @if (isset($page->id))
        <option value="0" @if ($page->image_show == 0) selected="" @endif>Не опубликовано</option>
        <option value="1" @if ($page->image_show == 1) selected="" @endif>Опубликовано</option>
    @else
        <option value="0">Не опубликовано</option>
        <option value="1">Опубликовано</option>
    @endif
</select>
<hr/>
<label for="">Мета Заголовок</label>
<input type="text" class="form-control" name="meta_title" placeholder="Мета заголовок"
       value="{{$page->meta_title ?? ""}}">

<label for="">Мета описание</label>
<input type="text" class="form-control" name="meta_description" placeholder="Мета описание"
       value="{{$page->meta_description ?? ""}}">

<label for="">Ключевые слова</label>
<input type="text" class="form-control" name="meta_keyword" placeholder="Ключевые слова"
       value="{{$page->meta_keyword ?? ""}}">

<hr/>
<input class="btn btn-primary" type="submit" value="Сохранить">
