@extends('admin.layouts.app_admin')

@section('content')

    <div class="container-fluid">

        @component('admin.components.breadcrumb')
            @slot('title') Создание страницы @endslot
            @slot('parent') Главная @endslot
            @slot('active') Страницы @endslot
        @endcomponent

        <hr/>

        <form class="form-horizontal" action="{{route('admin.page.store')}}" method="post"
              enctype="multipart/form-data">
            {{ csrf_field() }}

            {{-- Form include --}}
            @include('admin.pages.partials.form')
            <input type="hidden" name="created_by" value="{{Auth::id()}}">
        </form>
    </div>

@endsection
