@extends('admin.layouts.app_admin')

@section('content')

    <div class="container-fluid">

        @component('admin.components.breadcrumb')
            @slot('title') Список статей @endslot
            @slot('parent') Главная @endslot
            @slot('active')  Статьи @endslot
        @endcomponent

        <hr>
        <h4>Фильтры:</h4>
        <table class="table table-striped">
            <thead>
            <th>Поиск по названию</th>
            <th>Сортировка по категории</th>
            <th>Сортировка по публикации</th>

            </thead>
            <tbody>
            <td>
                <form class="form-inline my-2 my-md-0" action="{{route('admin.page.index')}}" method="get">
                    <input name="searching" class="form-control" type="text" placeholder="Введите запрос..."
                           aria-label="Search">
                    <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                </form>
            </td>
            <td>
                <form class="form-inline my-2 my-md-0" action="{{route('admin.page.index')}}" method="get">
                    <select class="form-control" name="category_search">
                        <option value="0">-- Все категории --</option>

                        @forelse ($categories as $category)


                            <option value="{{$category->id ?? ""}}"
                            >
                                {{$category->title ?? ""}}
                            </option>

                        @empty
                        @endforelse
                    </select>
                    <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                </form>
            </td>
            <td>
                <form class="form-inline my-2 my-md-0" action="{{route('admin.page.index')}}" method="get">
                    <select class="form-control" name="category_published">

                        <option value="0">Не опубликовано</option>
                        <option value="1">Опубликовано</option>

                    </select>
                    <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                </form>
            </td>
            </tbody>
        </table>
        <hr>
        <a href="{{route('admin.page.create')}}" class="btn btn-primary pull-right"><i
                    class="fa fa-plus-square-o"></i> Создать статью</a>
        <table class="table table-striped">
            <thead>
            <th>Наименование</th>
            <th>Публикация</th>
            <th>Изображение</th>
            <th>Категория</th>
            <th class="text-right">Действие</th>
            </thead>
            <tbody>
            @forelse ($pages as $page)
                <tr>
                    <td>{{$page->title}}</td>

                    <td>{{$page->published}}</td>
                    <td width="250"><img src="/storage/{{ $page->image }}" alt=""></td>
                    <td>{{$page->categories()->pluck('title')->implode(', ')}}</td>
                    <td class="text-right">
                        <form onsubmit="if(confirm('Удалить?')){return true }else{return false}"
                              action="{{route('admin.page.destroy', $page)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            {{csrf_field()}}
                            <a class="btn btn-default" href="{{route('admin.page.edit', $page)}}"><i
                                        class="fa fa-edit"></i></a>
                            <button class="btn" type="submit"><i class="fa fa-trash-o"></i></button>
                        </form>


                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center"><h2>Данные отсутствуют</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <td colspan="5">
                <ul class="pagination pull-right">
                    {{$pages->links()}}
                </ul>
            </td>
            </tfoot>
        </table>
    </div>

@endsection
