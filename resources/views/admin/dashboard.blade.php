@extends('admin.layouts.app_admin')

@section('content')
    <a class="container">
        <div class="row">
            <a class="col-sm-3" href="{{route('admin.category.index')}}">
                <div class="jumbotron">
                    <p><span class="label label-primary">Категорий {{$count_categories}}</span></p>
                </div>
            </a>
            <a class="col-sm-3" href="{{route('admin.category.index')}}">
                <div class="jumbotron">
                    <p><span class="label label-primary">Новостей {{$count_articles}}</span></p>
                </div>
            </a>
            <a class="col-sm-3" href="{{route('admin.page.index')}}">
                <div class="jumbotron">
                    <p><span class="label label-primary">Страниц {{$count_pages}}</span></p>
                </div>
            </a>
            @if(Auth::user()->isAdministrator())
                <a class=" col-sm-3" href="{{route('admin.user_managment.user.index')}}">
                <div class="jumbotron">
                    <p><span class="label label-primary">Пользователей {{$count_users}}</span></p>
                </div>
                </a>
            @endif
        </div>

        <div class="row">
            @if(Auth::user()->isAdministrator())
            <div class="col-sm-6">
                <h3>Категории</h3>
                <hr>
                <a class="btn btn-primary btn-default" href="{{route('admin.category.create')}}">Создать категорию</a>
                <hr>
                @foreach($categories as $category)


                    <a class="list-group-item" href="{{route('admin.category.edit', $category)}}">
                        <h4 class="list-group-item-heading">{{$category->title}}</h4>
                    <p class="list-group-item-text">
                        Количество материалов в категории: {{$category->articles()->count()}}
                    </p>
                </a>
                @endforeach
            </div>
            @else

                <div class="col-sm-6">
                    <h3>Страницы</h3>
                    <hr>
                    <a class="btn btn-primary  btn-default" href="{{route('admin.page.create')}}">Создать статью</a>
                    <hr>
                    @foreach($pages as $page)
                        <a class="list-group-item" href="{{route('admin.page.edit', $page)}}">
                            <h4 class="list-group-item-heading">{{$page->title}}</h4>
                            <p class="list-group-item-text">
                                Вложена в: {{$page->categories()->pluck('title')->implode(', ')}}
                            </p>
                        </a>
                    @endforeach
                </div>
            @endif

            <div class="col-sm-6">
                <h3>Новости</h3>
                <hr>
                <a class="btn btn-primary  btn-default" href="{{route('admin.article.create')}}">Создать новость</a>
                <hr>
                @foreach($articles as $article)
                    <a class="list-group-item" href="{{route('admin.article.edit', $article)}}">
                        <h4 class="list-group-item-heading">{{$article->title}}</h4>
                        <p class="list-group-item-text">
                            Категории: {{$article->categories()->pluck('title')->implode(', ')}}
                        </p>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection


