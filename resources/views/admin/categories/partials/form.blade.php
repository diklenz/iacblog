@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
<label for="">Статус</label>
<select class="form-control" name="published">
    @if (isset($category->id))
        <option value="0" @if ($category->published == 0) selected="" @endif>Не опубликовано</option>
        <option value="1" @if ($category->published == 1) selected="" @endif>Опубликовано</option>
    @else
        <option value="0">Не опубликовано</option>
        <option value="1">Опубликовано</option>
    @endif
</select>

<label for="">Наименование</label>
<input type="text" class="form-control" name="title" placeholder="Заголовок категории"
       value="{{$category->title ?? ""}}" required>

<label for="">Slug</label>
<input class="form-control" type="text" name="slug" placeholder="Автоматическая генерация"
       value="{{$category->slug ?? ""}}" readonly="">
<hr/>
<label for="">Изображение</label>
@if (isset($category->image))
    <img class="img-fluid" name="image_img" src="{{asset('/storage/'. $category->image ?? "")}}">
@endif
<input type="file" class="form-control" name="image">
<label for="">Показ изображения:</label>
<select class="form-control" name="image_show">
    @if (isset($category->id))
        <option value="0" @if ($category->image_show == 0) selected="" @endif>Не опубликовано</option>
        <option value="1" @if ($category->image_show == 1) selected="" @endif>Опубликовано</option>
    @else
        <option value="0">Не опубликовано</option>
        <option value="1">Опубликовано</option>
    @endif
</select>
<hr/>
<label for="">Родительская категория</label>
<select class="form-control" name="parent_id">
    <option value="0">-- без родительской категории --</option>
    @include('admin.categories.partials.categories', ['categories' => $categories])
</select>

<hr/>

<input class="btn btn-primary" type="submit" value="Сохранить">
