@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">

        @component('admin.components.breadcrumb')
            @slot('title') Список ролей @endslot
            @slot('parent') Главная @endslot
            @slot('active')  Пользователи @endslot
        @endcomponent


        <a href="{{route('admin.user_managment.role.create')}}" class="btn btn-primary pull-right my-4"><i
                    class="fa fa-plus-square-o "></i> Создать роль</a>

        <table class="table table-striped ">
            <thead>
            <th>Имя</th>
            <th class="text-right">Действие</th>
            </thead>
            <tbody>
            @forelse ($roles as $role)
                <tr>
                    <td>{{$role->name}}</td>

                    <td class="text-right">
                        <form onsubmit="if(confirm('Удалить?')){return true }else{return false}"
                              action="{{route('admin.user_managment.role.destroy', $role)}}" method="post">
                            {{method_field('DELETE')}}
                            {{csrf_field()}}
                            <a class="btn btn-default" href="{{route('admin.user_managment.role.edit', $role)}}"><i
                                        class="fa fa-edit"></i></a>
                            <button class="btn" type="submit"><i class="fa fa-trash-o"></i></button>
                        </form>


                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center"><h2>Данные отсутствуют</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <td colspan="5">
                <ul class="pagination pull-right">
                    {{$roles->links()}}
                </ul>
            </td>
            </tfoot>
        </table>
    </div>

@endsection
