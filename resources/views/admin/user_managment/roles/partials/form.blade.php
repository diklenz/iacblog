@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

<label for="">Название роли</label>
<input type="text" class="form-control" name="name" placeholder="Название"
       value="@if(old('name')){{old('name')}}@else{{$role->name ?? ""}}@endif" required>


<hr/>

<input class="btn btn-primary" type="submit" value="Сохранить">