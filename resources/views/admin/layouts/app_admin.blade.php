<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token " content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body id="app">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">

        <a class="navbar-brand" href="{{ url('/admin') }}">СПБ ИАЦ</a>
        @guest
            <ul class="navbar-nav px-3 navbar-nav mr-auto">
                @else
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07"
                aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

            <div class="collapse navbar-collapse" id="navbarsExample07">

                <ul class="navbar-nav px-3 navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.index')}}">Панель состояния</a>
                    </li>
                    @if(Auth::user()->isAdministrator())
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('admin.category.index')}}">Категории</a>
                        </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.article.index')}}">Новости</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.page.index')}}">Страницы</a>
                    </li>


                    @if(Auth::user()->isAdministrator())

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="admin.user_managment.user.index"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Управление
                            пользователями</a>
                        <div class="dropdown-menu" aria-labelledby="admin.user_managment.user.index">

                            <a class="dropdown-item"
                               href="{{route('admin.user_managment.user.index')}}">Пользователи</a>

                            <a class="dropdown-item"
                               href="{{route('admin.user_managment.role.index')}}">Роли</a>
                        </div>
                    </li>
                    @endif

                @endguest
                    <li class="nav-item">
                        <a target="_blank" class="nav-link" href="{{ url('/') }}">Перейти на сайт</a>
                    </li>
                </ul>
                <ul class="navbar-nav px-3 ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <!--li class="nav-item text-nowrap">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Войти') }}</a>
                        </li-->

                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Выйти') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
    </div>
</nav>


<div class="container">


    <main role="main">
        <div class="col-sm-12 py-4">
            @yield('content')
        </div>


    </main>
</div>


</body>


<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
