
@foreach ($pages as $page)
    @if($page->categories()->count())
    @else
        <li>
            <a href="{{url("/blog/page/$page->slug")}}" class="nav-link ">{{$page->title}} </a>

        </li>
    @endif
@endforeach
