<header>
    <a class="logo" href="{{ url('/') }}"><img class="w60 p5" src="/images/logo-white.png"
                                               alt=" {{ config('app.name', 'Laravel') }}"></a>

    <div class="right-area">
        <form class="src-form" action="/blog/search" method="get">
            <button type="submit"><i class="ion-search"></i></a></button>
            <input type="text" name="searching" aria-label="Search" placeholder="Поиск новостей">
        </form><!-- src-form -->

    </div><!-- right-area -->

    <a class="menu-nav-icon" data-menu="#main-menu" href="#"><i class="ion-navicon"></i></a>

    <ul class="main-menu" id="main-menu">
        @include('layouts.top_menu', ['categories'=> $categories])
        @include('layouts.top_menu_page', ['categories'=> $categories, 'pages'=> $pages])

    </ul>


    <div class="clearfix"></div>
</header>

