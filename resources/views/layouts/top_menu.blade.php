@foreach ($categories as $category)
    @if ($category->title == "Главная")

    @elseif ($category->children->where('published', 1)->count())
        <li class="dropdown">
            <a href="{{url("/blog/category/$category->slug")}}" class="nav-link  dropdown-toggle" data-toggle="dropdown"
               role="button" aria-expanded="false">
                {{$category->title}} <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                @include('layouts.top_menu', ['categories' => $category->children])
            </ul>
    @else
        @if ($category->pages->count())
            <li>
                <a href="{{url("/blog/category/$category->slug")}}" class="nav-link  dropdown-toggle"
                   data-toggle="dropdown"
                   role="button" aria-expanded="false">{{$category->title}} </a>
                <ul class="dropdown-menu" role="menu">
                    @foreach ($category->pages as $page)

                        <li>
                            <a href="{{url("/blog/page/$page->slug")}}" class="nav-link ">{{$page->title}}</a>

                        </li>

                    @endforeach
                </ul>
        @else
        <li>
            <a href="{{url("/blog/category/$category->slug")}}" class="nav-link ">{{$category->title}} </a>
            @endif
            @endif
        </li>
        @endforeach
