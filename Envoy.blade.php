@setup
$user = 'diklenz';
$timezone = 'Europe/Moscow';

$path = '/var/www/blog';

$current = $path . '/current';
$storage = $path . '/uploads';
$repo = 'git@bitbucket.org:diklenz/iacblog.git';

$branch = 'bitbucket';

$chmods = [
'storage/logs'
];

$date = new DateTime('now', new DateTimeZone($timezone));
$release = $path . '/releases/' . $date->format('YndHis');
$release_storage = $current . '/storage/app/public';
@endsetup

@servers (['production' => $user . '@46.229.212.189'])

@task('clone', ['on'=> $on])
mkdir -p {{$release}}

git clone --depth 1 -b {{$branch}} "{{$repo}}" {{$release}}

echo "#1 - Repository has been cloned"
@endtask

@task('composer', ['on' => $on])

cd {{$release}}
composer install --no-interaction --no-dev --prefer-dist

echo "#2 Composer dependencies have been installed"
@endtask

@task('artisan', ['on' => $on])
cd {{$release}}

ln -nfs {{$path}}/.env .env;
chgrp -h www-data .env;

php artisan config:clear

php artisan migrate

php artisan clear-compiled --env=production;
php artisan optimize --env=production;
php artisan storage:link;
php artisan vendor:publish --tag=ckeditor

echo "#3 - Production dependencies have been installed"
@endtask

@task ('chmod', ['on' => $on])
chgrp -R www-data {{$release}};
chmod -R ug+rwx {{$release}};
chmod -R 775 {{$release}};
chmod -R 775 {{$release}}/storage;
chmod -R 775 {{$release}}/storage/app;
chmod -R 775 {{$release}}/storage/app/public;
chmod -R 775 {{$release}}/storage/framework;
chmod -R 775 {{$release}}/storage/logs;
chmod -R 775 {{$release}}/bootstrap/cache;

chown -R {{$user}}:www-data  {{$release}}/storage;
chown -R {{$user}}:www-data  {{$release}}/storage/app/public;
chown -R {{$user}}:www-data  {{$release}}/bootstrap/cache;

@foreach($chmods as $file)
    chmod -R 755 {{$release}}/{{$file}}

    chown -R {{$user}}:www-data {{$release}}/{{$file}}

    echo "Permissions have been set for {{$file}}"
@endforeach

echo "#4 - Permissions has been set"
@endtask

@task('update_symlinks')
ln -nfs {{$release}} {{$current}};
chgrp -h www-data {{$current}};
echo "#5 - Symlink /current has been set"

@endtask

@task('symlink_uploads')

cd {{$release_storage}}

ln -nfsv {{$path}}/uploads {{$release_storage}}/uploads;
chgrp -h www-data {{$release_storage}}/uploads;

echo "#6 - Symlink /uploads has been set"
@endtask

@macro('deploy', ['on' => 'production'])
clone
composer
artisan
chmod
update_symlinks
symlink_uploads
@endmacro


